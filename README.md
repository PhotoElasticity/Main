
Photoelasticity's Wiki
======================

The aim of this project is to enable people to work on this wiki conveniently. 
It provides important guidelines for adding content properly, which will later be deployed on the static website [photoelasticity.net](https://photoelasticity.net/). Before working on the wiki, please read the following guidelines carefully.

Recommendation to editors of wiki:
----------------------------------

##### Page naming

If you want to add a new page or rename an existing one, please, DO NOT EVER use spaces (or strange characters) in the name of the page, which will be used as an internal reference in the wiki.

Also, if you add a new page, remember to add it in the navigation menu as explained below

##### LateX equation

At time of testing there was an incompatibility between Latex tags in the wiki style and 
mkdocs (the tool which automatically converts the wiki to as static web site).

Thus all \`\`\`math tags must be changed to `$$`
And the patterns \$\` and \`\$ must be replace with `\(` and `\)` respectively.

So to add an equation simply write: 
```markdown
\( my_equation_in_classical_latex \)
```

##### Internal links

When referencing to another page of the wiki, use absolute paths, for example write:
```markdown
*  Silicone mold with cylindrical photoelastic particles done with [this](/molding-urethane):
```
and NOT
```markdown
*  Silicone mold with cylindrical photoelastic particles done with [this](molding-urethane):
```

Also, to reference the starting page reference to the `/home` one explicitely:
```markdown
[<go back to home](/home)
```
and NOT
```markdown
[<go back to home](/)
```

##### Image insertion

If you want to add an image in your page, please uses wiki insertion with relative path and not html reference:
```markdown
![img](uploads/7e9d15a3fc844774cb987ea3fb530f0e/Force_measurement.svg){width=800}
```
and NOT
```markdown
<img src="uploads/7e9d15a3fc844774cb987ea3fb530f0e/Force_measurement.svg"  width="800">
```

##### Updating the navigation menu

Contrary to the wiki, the navigation menu of the endpoint website is defined
in the [`mkdocs.yml`](https://git-xen.lmgc.univ-montp2.fr/PhotoElasticity/Main/-/blob/master/mkdocs.yml?ref_type=heads) file of the current directory. Have a look to the `nav` section of the file, you will easily understand how it works.

##### Author contribution

If you modify the wiki, please remember to add your name and institution logo to the [contributor's list](/list-people) 

Deployement:
------------

Ones you are done editing the wiki, you want your modifications to be deployed on the static website [photoelasticity.net](https://photoelasticity.net/). But before doing so you want to check that it appears how you expected.

#### Testing the result:

It is possible to generate a preview of the obtained website. On the left menu bar, access the 
`Build -> Pipelines` menu. Then click on the `Run pipeline` blue button on the upper right part
of the page. Finally click the `Run pipeline` blue button again.

After about 3 minutes, the `deploy` created box should have green icon. You can then access
the preview website on : (https://photoelasticity.pages-git-xen.lmgc.univ-montp2.fr/Main/).

You can also find this link from the `Deploy -> Pages` left menu bar.

#### Deploy

Now that you are happy with your modifications and how they appear in the 'virtual' final version, you want to deploy it on the static website. For security reason it is currently only doable by contacting @mozul. So please do it!



Oldies:
-------


#### Generate a static website:

To generate a static website the first thing is to clone
the wiki part of the project:

```shell
git clone git@git-xen.lmgc.univ-montp2.fr:PhotoElasticity/Main.wiki.git
```

Then, due to historical and technical reason, there may be some files
missing in the *uploads* subdirectory. To fix this, use the *missing_images.py*
python script (check the input paths beforehand).


Finally the [mkdocs](https://www.mkdocs.org/) utilities is used to generate/present
the static website.

Python is needed to install with:
```shell
python -m pip install mkdocs python-markdown-math
```

Firstly, the *home.md* must be renamed to *index.md* for mkdocs to work.
```shell
mv home.md index.md
```

Then the site can be checked locall by serving it with:
```
mkdocs serve
```

Or it can be build and be served by any other web server:
```
mkdocs build
```

#### Hosting of website:

Currently the website is hosted on the `HBextranet` virtual machine of the
LMGC.

To update the website, copy the previously generated `site` directory in
the `/var/www/html/photoelasticity` directory of the server (do not forget
to put the correct rights/owner).

Then make sure that the `photoelasticity` file, used to configure the
correspondig nginx web server if correct, enabled in nginx before
restartig the service.

Currently a certificate generated with certbot/letsencrypt is issued.
Do not forget to renew or update configuration in the future in case
of migration.
