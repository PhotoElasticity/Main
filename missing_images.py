import re
import urllib.request
from pathlib import Path

wiki = Path('Main.wiki')
wiki_url = "https://git-xen.lmgc.univ-montp2.fr/PhotoElasticity/Main/uploads/"

uploads = wiki/'uploads'

# look for image name in uploads directory
found_images = []
for obj in uploads.rglob("*"):
  if obj.is_file():
    found_images.append(obj.name)


# list all .md files
md_files = []
for obj in wiki.rglob("*.md"):
  if obj.is_file():
    md_files.append(obj)


# look for all image names in all .md files
fimage = re.compile(r"uploads/(?P<fname>\w*/.*)\)")
images = []
for md_file in md_files:
  with open(md_file, 'r') as f:
    lines = f.read()
    found = fimage.findall(lines)
    images.extend(found)


# look for missing images
missing = []
for im in images:
  name = im.split('/')[-1]
  if name not in found_images:
    missing.append(im)
    

# download the missing images
for m in missing:
    print( 'downloading missing image : ', m )
    new = uploads/m
    new.parent.mkdir(exist_ok=True)
    m = urllib.parse.quote(m)
    urllib.request.urlretrieve(wiki_url+m, str(new))
